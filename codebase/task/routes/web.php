<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Home route
Route::get('/', function () {
    return view('welcome');
});

// API routes for tasks
Route::controller(TaskController::class)->prefix('/tasks')->group(function () {
    Route::get('/', 'index')->name('task.index');
    Route::post('/', 'store')->name('task.store');
    Route::get('/{id}', 'show')->name('task.show');
    Route::patch('/{id}', 'update')->name('task.update');
    Route::delete('/{id}', 'destroy')->name('task.destroy');
});


