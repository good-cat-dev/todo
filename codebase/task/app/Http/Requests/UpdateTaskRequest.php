<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'status' => 'required|numeric|in:0,1',
            'priority' => 'required|numeric|in:1,2,3,4,5',
            'parent_id'=> 'nullable|exists:tasks,id',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Required field',
            'title.string' => 'Need to be string',
            'description.required' => 'Required field',
            'description.string' => 'Need to be string',
            'status.required' => 'Required field',
            'status.numeric' => 'Need to be numeric',
            'status.in' => 'Value 0 or 1',
            'priority.required' => 'Required field',
            'priority.numeric' => 'Need to be numeric',
            'priority.in' => 'Value from 1 to 5',
            'parent_id.exists' => 'No such tasks with current ID',
        ];
    }

    /**
     * @param Validator $validator
     *
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'The given data was invalid.',
            'errors' => $validator->errors(),
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
