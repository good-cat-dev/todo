<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Services\TaskService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TaskController extends Controller
{
    /**
     * @param Request $request
     * @param TaskService $taskService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( Request $request, TaskService $taskService )
    {
        try {
            return response()->json( $taskService->fetch( $request ) );
        } catch ( Exception $e ) {
            Log::error( 'TASK INDEX ERROR', [ 'message' => $e->getMessage() ] );

            return response()->json( [ 'error' => $e->getMessage() ], 500 );
        }
    }

    /**
     * @param StoreTaskRequest $request
     * @param TaskService $taskService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( StoreTaskRequest $request, TaskService $taskService )
    {
        try {
            return response()->json( $taskService->create( $request ) );
        } catch ( Exception $e ) {
            Log::error( 'TASK CREATE ERROR', [ 'message' => $e->getMessage() ] );

            return response()->json( [ 'error' => $e->getMessage() ], 500 );
        }
    }

    /**
     * @param $id
     * @param TaskService $taskService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( $id, TaskService $taskService )
    {
        try {
            return response()->json( $taskService->find( $id ) );
        } catch ( ModelNotFoundException ) {
            return response()->json( [ 'error' => 'Entity not found' ], 404 );
        } catch ( Exception $e ) {
            Log::error( 'TASK SHOW ERROR', [ 'message' => $e->getMessage() ] );

            return response()->json( [ 'error' => $e->getMessage() ], 500 );
        }
    }

    /**
     * @param UpdateTaskRequest $request
     * @param $id
     * @param TaskService $taskService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( UpdateTaskRequest $request, $id, TaskService $taskService )
    {
        try {
            return response()->json( $taskService->update($request, $id) );
        } catch ( Exception $e ) {
            Log::error( 'TASK UPDATE ERROR', [ 'message' => $e->getMessage() ] );

            return response()->json( [ 'error' => $e->getMessage() ], 500 );
        }
    }

    /**
     * @param $id
     * @param TaskService $taskService
     *
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function destroy( $id, TaskService $taskService )
    {
        try {
            if ($taskService->destroy($id)){
                return response()->json('The task was successfully deleted');
            }
        } catch ( ModelNotFoundException ) {
            return response()->json( [ 'error' => 'Entity not found' ], 404 );
        }  catch ( Exception $e ) {
            Log::error( 'TASK DELETE ERROR', [ 'message' => $e->getMessage() ] );

            return response()->json( [ 'error' => $e->getMessage() ], 500 );
        }
    }
}
