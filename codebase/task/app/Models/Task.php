<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status', 'priority', 'title', 'description'];

    /**
     * @param $value
     *
     * @return string|void
     */
    public function getStatusAttribute($value)
    {
        if ($value === 0) {
            return 'Todo';
        } elseif ($value === 1) {
            return 'Done';
        }
    }

    /**
     * Get the subtasks associated with the task.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subtasks()
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    /**
     * Check if the task has uncompleted subtasks.
     *
     * @return bool
     */
    public function hasUncompletedSubtasks()
    {
        return $this->subtasks()->where('status', 'todo')->exists();
    }

    /**
     * Get the parent task of the subtask.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentTask()
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    /**
     * Check if the task is a subtask.
     *
     * @return bool
     */
    public function isSubtask()
    {
        return $this->getAttribute('is_subtask');
    }
}

