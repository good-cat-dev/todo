<?php

namespace App\Filters;

use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;

class TaskFilter
{
    private const AVAILABLE_SORTING_FIELDS = ['created_at', 'updated_at', 'priority'];

    public function apply(array $filters)
    {
        $query = Task::query();

        if (isset($filters['status'])) {
            $status = $filters['status'];
            $query->when($status === 'todo', function ($q) {
                return $q->where('status', 0);
            })
                  ->when($status === 'done', function ($q) {
                      return $q->where('status', 1);
                  });
        }

        if (isset($filters['priority'])) {
            $priority = $filters['priority'];
            if (strpos($priority, '-') !== false) {
                $priorityRange = explode('-', $priority);
                $minPriority = $priorityRange[0] ?? null;
                $maxPriority = $priorityRange[1] ?? null;

                if ($minPriority !== null) {
                    $query->where('priority', '>=', $minPriority);
                }

                if ($maxPriority !== null) {
                    $query->where('priority', '<=', $maxPriority);
                }
            } else {
                $query->where('priority', $priority);
            }
        }

        if (isset($filters['title'])) {
            $title = $filters['title'];
            $query->where('title', 'like', "%{$title}%");
        }

        if (isset($filters['sort'])) {
            $this->addSorting($filters['sort'], $query);
        }

        return $query;
    }

    private function addSorting(string $fieldForSorting, Builder &$query)
    {
        $preparedField = str_replace('-','', $fieldForSorting);
        if (in_array($preparedField, self::AVAILABLE_SORTING_FIELDS)){
            if ($preparedField !== $fieldForSorting) {
                $query->orderByDesc($preparedField);
            } else {
                $query->orderBy($preparedField);
            }
        }
    }
}
