<?php

namespace App\Services;

use App\Filters\TaskFilter;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TaskService
{
    /**
     * @param TaskFilter $taskFilter
     */
    public function __construct(TaskFilter $taskFilter)
    {
        $this->taskFilter = $taskFilter;
    }

    /**
     * @param Request $request
     *
     * @return Collection
     */
    public function fetch( Request $request ):Collection
    {
        return $this->taskFilter->apply( $request->all() )->get();
    }

    /**
     * @param StoreTaskRequest $request
     *
     * @return Task
     */
    public function create( StoreTaskRequest $request ): Task
    {
        $task              = new Task();
        $task->title       = $request->input( 'title' );
        $task->description = $request->input( 'description' );
        $task->status      = $request->input( 'status' );
        $task->priority    = $request->input( 'priority' );
        // TODO Save parents ids as string. For example parent_id = "1/2/3"
        $task->parent_id   = $request->input( 'parent_id' );
        $task->is_subtask  = $request->input( 'parent_id' ) ? true : false;

        $task->save();

        return $task->refresh();
    }

    /**
     * @param $id
     *
     * @return Task
     */
    public function find( $id ): Task
    {
        return Task::findOrFail( $id );
    }

    /**
     * @param UpdateTaskRequest $request
     * @param $id
     *
     * @return mixed
     * @throws Exception
     */
    public function update( UpdateTaskRequest $request, $id )
    {
        $task = Task::findOrFail( $id );
        // TODO move to request validation
        if ( $request->input( 'status' ) == 1 && $task->hasUncompletedSubtasks() ) {
            throw new Exception('Unable to mark the task as done because it has uncompleted subtasks');
        }

        $task->title       = $request->input( 'title' );
        $task->description = $request->input( 'description' );
        $task->status      = $request->input( 'status' );
        $task->priority    = $request->input( 'priority' );
        $task->parent_id   = $request->input( 'parent_id' );
        $task->is_subtask  = $request->input( 'parent_id' ) ? true : false;
        $task->updated_at  = Carbon::now();
        $task->save();

        return $task->refresh();
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function destroy( $id ): bool
    {
        $task = Task::findOrFail( $id );

        if ( $task->status == 'Done' ) {
            throw new Exception('Unable to delete the task because it has a status of DONE.');
        }

        $result = $task->delete();

        if (!$result) {
            throw new Exception('Unable to delete entity');
        }

        return $result;
    }

}


